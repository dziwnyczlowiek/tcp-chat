﻿using SharedClassLibrary;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Text.Json.Serialization;

namespace ChatServer.Models
{
    public class ActiveUser : User
    {
        public TcpClient TcpClient;
        public ActiveUser(int id, string username, TcpClient tcpClient) : base(id, username)
        {
            TcpClient = tcpClient;
        }
    }
}
