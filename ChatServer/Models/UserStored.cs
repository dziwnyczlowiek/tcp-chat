﻿using SharedClassLibrary;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace ChatServer.Models
{
    public class UserStored : User
    {
        private static int _nextId = 0;
        public string Password { get; private set; }

        public UserStored(string username, string password, TcpClient client) : base(_nextId, username)
        {
            _nextId++;
            Password = password;
        }

        public User GetUserInfo() => new User(Id, Username);
    }
}
