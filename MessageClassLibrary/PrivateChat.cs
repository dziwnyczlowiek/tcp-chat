﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace SharedClassLibrary
{
    public class PrivateChat
    {
        private static int _nextId = 0;

        private HashSet<User> _users = new HashSet<User>();
        public List<User> Users { get => new List<User>(_users); }
        public HashSet<User> HashUsers { get => _users; private set => _users = value; }

        public delegate void UsersChangedEventHandler(object sender, UserChangedEventArgs args);
        public event UsersChangedEventHandler UsersChanged;

        public int Id { get; }

        public PrivateChat()
        {
            Id = _nextId;

            _nextId++;
        }

        public PrivateChat(int id)
        {
            Id = id;
        }

        [JsonConstructor]
        public PrivateChat(int id, HashSet<User> hashUsers)
        {
            Id = id;
            HashUsers = hashUsers;
        }

        public bool AddUser(User addedUser)
        {
            foreach (User checkedUser in _users)
            {
                if (checkedUser.Id == addedUser.Id)
                    return false;
            }

            _users.Add(addedUser);

            UsersChanged?.Invoke(this, new UserChangedEventArgs(addedUser, null));

            return true;
        }

        public bool RemoveUser(User removedUser)
        {
            foreach (User checkedUser in _users)
            {
                if (checkedUser.Id == removedUser.Id)
                {
                    // Event występuje przed usunięciem użytkownika
                    UsersChanged?.Invoke(this, new UserChangedEventArgs(null, checkedUser));
                    _users.Remove(checkedUser);
                    return true;
                }
            }

            return false;
        }

        public List<User> GetUsers() => new List<User>(_users);

        public class UserChangedEventArgs : EventArgs
        {
            public User newUser { get; }
            public User oldUser { get; }

            public UserChangedEventArgs(User newUser, User oldUser)
            {
                this.newUser = newUser;
                this.oldUser = oldUser;
            }
        }
    }
}
