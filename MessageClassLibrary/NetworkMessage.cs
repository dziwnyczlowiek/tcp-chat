﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace SharedClassLibrary
{
    public class NetworkMessage
    {
        public int ResponseCode { get; private set; }
        public string Message { get; private set; }
        public JArray Load { get; private set; }

        [JsonConstructor]
        public NetworkMessage(int responseCode, string message, object[] load)
        {
            ResponseCode = responseCode;

            Message = message;

            if (load != null)
                Load = JArray.FromObject(load);
            else
                Load = new JArray();
        }

        public NetworkMessage(int responseCode, string message, object load) : this(responseCode, message, new object[] { load }) { }
        public NetworkMessage(int responseCode, string message) : this(responseCode, message, null) { }
    }
}
