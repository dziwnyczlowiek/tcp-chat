﻿namespace SharedClassLibrary
{
    public class User
    {
        public int Id { get; }
        public string Username { get; }

        public User(int id, string username)
        {
            Id = id;
            Username = username;
        }
    }
}