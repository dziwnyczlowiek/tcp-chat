﻿using ChatClient.Messages;
using ChatClient.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.DependencyInjection;
using Microsoft.Toolkit.Mvvm.Messaging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace ChatClient.ViewModels
{
    public class MainWindowViewModel : ObservableRecipient
    {
        private IServerService _serverService = Ioc.Default.GetService<IServerService>();

        private ObservableObject _currentViewModel;
        public ObservableObject CurrentViewModel
        {
            get => _currentViewModel;
            set => SetProperty(ref _currentViewModel, value);
        }

        private Dictionary<string, ObservableObject> _activeViewsModels = new Dictionary<string, ObservableObject>();

        public MainWindowViewModel()
        {
            // Utworzenie pierwszego ViewModelu i przechowanie go w słowniku aktywnych ViewModeli
            LoginViewModel initialViewModel = new LoginViewModel();
            _currentViewModel = initialViewModel;
            _activeViewsModels.Add(nameof(initialViewModel), initialViewModel);

            // Rejestracje do otrzymywania wiadomości
            Messenger.Register<MainWindowViewModel, UIMessage>(this, (r, m) => r.OnReceiveMessage(m));
        }

        private void OnReceiveMessage(UIMessage message)
        {
            switch (message.EventType)
            {
                case UIMessage.UIEvent.LoggedIn:
                    OnLoggedIn();
                    break;
                case UIMessage.UIEvent.Disconnected:
                    OnDisconnected();
                    break;
            }
            
        }

        private void OnDisconnected()
        {
            // Usuń aktywne ViewModele
            _activeViewsModels.Clear();

            // Zmień ViewModel na login
            ChangeCurrentViewModel(typeof(LoginViewModel));
        }

        private void OnLoggedIn()
        {
            // Zmień ViewModel na czat
            ChangeCurrentViewModel(typeof(ChatViewModel));
        }

        private void ChangeCurrentViewModel(Type viewModelClass)
        {
            if (_activeViewsModels.ContainsKey(viewModelClass.Name))
            {
                CurrentViewModel = _activeViewsModels[viewModelClass.Name];
            }
            else
            {
                ObservableObject viewModel = (ObservableObject)Activator.CreateInstance(viewModelClass);
                _activeViewsModels.Add(viewModelClass.Name, viewModel);
                CurrentViewModel = viewModel;
            }
        }
        
    }
}
