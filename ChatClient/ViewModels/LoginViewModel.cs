﻿using ChatClient.Messages;
using ChatClient.Services;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.DependencyInjection;
using Microsoft.Toolkit.Mvvm.Input;
using Microsoft.Toolkit.Mvvm.Messaging;
using SharedClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

namespace ChatClient.ViewModels
{
    public class LoginViewModel : ObservableValidator
    {
        private readonly IServerService _serverService = Ioc.Default.GetService<IServerService>();

        private string _errorMessage;
        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }

        bool _isUsernameWrong;
        public bool IsUsernameWrong
        {
            get => _isUsernameWrong;
            set => SetProperty(ref _isUsernameWrong, value);
        }

        bool _isPasswordWrong;
        public bool IsPasswordWrong
        {
            get => _isPasswordWrong;
            set => SetProperty(ref _isPasswordWrong, value);
        }

        private string _username;
        [Required]
        [MaxLength(16)]
        [MinLength(1)]
        public string Username {
            get => _username;
            set => SetProperty(ref _username, value);
        }

        public LoginViewModel()
        {
            // Logowanie i rejestracja
            LoginCommand = new RelayCommand<object>(Login);
            RegisterCommand = new RelayCommand<object>(Register);

            // Obsługa kontrolek
            UsernameBoxResetCommand = new RelayCommand(UsernameBoxReset);
            PasswordBoxResetCommand = new RelayCommand(PasswordBoxReset);

            Username = "";
        }

        // Komendy
        public ICommand LoginCommand { get; }

        private void Login(object passwordBox)
        {
            PasswordBox password = passwordBox as PasswordBox;

            bool passedValidation = ValidateCredentials(Username, password);

            // Jeśli poczatkowa weryfikacja się nie powiodła
            if (!passedValidation)
                return;

            NetworkMessage response = _serverService.LogInToServer(Username, password.Password);

            // Przeanalizuj odpowiedź
            ProcessResponse(response);
        }

        public ICommand RegisterCommand { get; }
        private void Register(object passwordBox)
        {
            PasswordBox password = passwordBox as PasswordBox;

            bool passedValidation = ValidateCredentials(Username, password);

            // Jeśli poczatkowa weryfikacja się nie powiodła
            if (!passedValidation)
                return;

            NetworkMessage response = _serverService.RegisterToServer(Username, password.Password);

            // Przeanalizuj odpowiedź
            ProcessResponse(response);
        }

        public ICommand UsernameBoxResetCommand { get; }
        private void UsernameBoxReset() => IsUsernameWrong = false;

        public ICommand PasswordBoxResetCommand { get; }
        private void PasswordBoxReset() => IsPasswordWrong = false;

        // Inne metody
        private bool ValidateCredentials(string username, PasswordBox passwordBox)
        {
            bool passedValidation = true;

            // Weryfikacja hasła
            if (passwordBox.Password == "")
            {
                passedValidation = false;
                IsPasswordWrong = true;
            }
            else
            {
                IsPasswordWrong = false;
            }

            // Weryfikacja nazwy użytkownika
            if (Username == "" || Username.Length > 16)
            {
                passedValidation = false;
                IsUsernameWrong = true;
            }
            else
            {
                IsUsernameWrong = false;
            }

            return passedValidation;
        }

        private void ProcessResponse(NetworkMessage response)
        {
            // Jeśli nie uzyskano odpowiedzi
            if (response == null)
            {
                ErrorMessage = "Nie udało się połączyć z serwerem";
                return;
            }

            // Jeśli logowanie się powiodło
            if (response.ResponseCode == ResponseCodes.LOGIN_SUCCESSFUL
                || response.ResponseCode == ResponseCodes.REGISTER_SUCCESSFUL)
            {
                // Wyślij wiadomość o pomyślnym zalogowaniu
                WeakReferenceMessenger.Default.Send(new UIMessage(UIMessage.UIEvent.LoggedIn));
            }
            else
            {
                ErrorMessage = response.Message;
            }
        }
    }
}
