﻿using ChatClient.Messages;
using ChatClient.Models;
using ChatClient.Services;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.DependencyInjection;
using Microsoft.Toolkit.Mvvm.Input;
using Microsoft.Toolkit.Mvvm.Messaging;
using SharedClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Windows.Data;
using System.Windows.Input;

namespace ChatClient.ViewModels
{
    public class ChatGlobalViewModel : ObservableRecipient
    {
        private IServerService _serverService = Ioc.Default.GetService<IServerService>();

        private ObservableCollection<ChatMessage> _chatMessages = new ObservableCollection<ChatMessage>();
        public ObservableCollection<ChatMessage> ChatMessages
        {
            get => _chatMessages;
            set => SetProperty(ref _chatMessages, value);
        }

        private ObservableCollection<User> _userList;

        public ObservableCollection<User> UserList
        {
            get => _userList;
            set => SetProperty(ref _userList, value);
        }

        private string _messageToSend;
        public string MessageToSend
        {
            get => _messageToSend;
            set => SetProperty(ref _messageToSend, value);
        }

        public ChatGlobalViewModel()
        {
            UserList = _serverService.ActiveUsers;

            SendMessageCommand = new RelayCommand(SendMessage);

            MessageToSend = "";

            Messenger.Register<ChatGlobalViewModel, UIMessage>(this, (r, m) => r.OnUserListUpdate(m));
            Messenger.Register<ChatGlobalViewModel, ReceivedMessage>(this, (r, m) => r.OnMessageReceived(m));
        }

        private void OnUserListUpdate(UIMessage message)
        {
            UserList = _serverService.ActiveUsers;
        }

        private void OnMessageReceived(ReceivedMessage receivedMessage)
        {
            if (receivedMessage.ChatroomId == -1)
                ChatMessages.Add(receivedMessage.Message);
        }

        // Komendy
        public ICommand SendMessageCommand { get; set; }
        private void SendMessage()
        {
            NetworkMessage networkMessage = new NetworkMessage(ResponseCodes.SEND_GLOBAL_MESSAGE, MessageToSend, _serverService.LoggedInUser);

            _serverService.SendMessage(networkMessage);

            MessageToSend = "";
        }
    }
}
