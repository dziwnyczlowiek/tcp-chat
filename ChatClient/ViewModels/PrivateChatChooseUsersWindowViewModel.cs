﻿using ChatClient.Services;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.DependencyInjection;
using Microsoft.Toolkit.Mvvm.Input;
using SharedClassLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;

namespace ChatClient.ViewModels
{
    public class PrivateChatChooseUsersWindowViewModel : ObservableObject
    {
        private IServerService _serverService = Ioc.Default.GetService<IServerService>();

        private ObservableCollection<User> _userList;
        public ObservableCollection<User> UserList
        {
            get => _userList;
            set => SetProperty(ref _userList, value);
        }

        private List<int> _checkedIdList = new List<int>();
        public List<int> CheckedIdList
        {
            get => _checkedIdList;
            set => SetProperty(ref _checkedIdList, value);
        }

        public PrivateChatChooseUsersWindowViewModel()
        {
            CheckUserCommand = new RelayCommand<int>(CheckUser);

            int currentUserId = _serverService.LoggedInUser.Id;

            UserList = new ObservableCollection<User>(_serverService.ActiveUsers);

            for (int i = 0; i < UserList.Count; i++)
            {
                if (UserList[i].Id == currentUserId)
                    UserList.RemoveAt(i);
            }

            _checkedIdList.Add(currentUserId);
        }

        // Komendy
        public ICommand CheckUserCommand { get; }
        private void CheckUser(int id)
        {
            if (_checkedIdList.Contains(id))
            {
                _checkedIdList.Remove(id);
            }
            else
            {
                _checkedIdList.Add(id);
            }
        }
    }
}
