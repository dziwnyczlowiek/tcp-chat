﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChatClient.Views
{
    /// <summary>
    /// Logika interakcji dla klasy ChatGlobalView.xaml
    /// </summary>
    public partial class ChatGlobalView : UserControl
    {
        public ChatGlobalView()
        {
            InitializeComponent();

            ((INotifyCollectionChanged)this.itemsControl.Items).CollectionChanged +=
                itemsControl_CollectionChanged;
        }

        private void itemsControl_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.scrollViewer.ScrollToBottom();
        }
    }
}
