﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ChatClient.Views
{
    /// <summary>
    /// Logika interakcji dla klasy PrivateChatChooseUsersWindowView.xaml
    /// </summary>
    public partial class PrivateChatChooseUsersWindowView : Window
    {
        public PrivateChatChooseUsersWindowView()
        {
            InitializeComponent();
        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void Accept(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Jeśli okno jest zamknięte inaczej niż przez przycisk to:
            //this.DialogResult = false;
        }
    }
}
