﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChatClient.Messages
{
    // Klasa wiadomości dla pomyślnego logowania
    public class UIMessage
    {
        public UIEvent EventType { get; }
        public string Message { get; }
        public UIMessage(UIEvent eventType)
        {
            EventType = eventType;
            Message = "";
        }

        public UIMessage(UIEvent eventType, string message)
        {
            EventType = eventType;
            Message = message;
        }

        public enum UIEvent
        {
            LoggedIn,
            LoggedOut,
            Disconnected,
            UserListUpdated,
            ChatListUpdated,
            PrivateChatClick,
            ReturnToChatList
        }
    }
}
